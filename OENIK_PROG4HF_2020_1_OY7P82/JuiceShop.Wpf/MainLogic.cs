﻿using GalaSoft.MvvmLight.Messaging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace JuiceShop.Wpf
{
    class MainLogic
    {
        string url = "http://localhost:65454/api/JuicesApi/";
        HttpClient client = new HttpClient();
        void SendMessage(bool succes)
        {
            string msg = succes ? "Operation completed successfully" : "Operation failed";
            Messenger.Default.Send(msg, "Juicesresult");
        }
        public List<JuiceVM> ApiGetModells()
        {
            string json = client.GetStringAsync(url + "all").Result;
            var list = JsonConvert.DeserializeObject<List<JuiceVM>>(json);
            return list;
        }
        public void ApiDelModell(JuiceVM modell)
        {
            bool success = false;
            if (modell != null)
            {
                string json = client.GetStringAsync(url + "del/" + modell.ID).Result;
                JObject obj = JObject.Parse(json);
                success = (bool)obj["OperationResult"];
            }
            SendMessage(success);
        }
        private bool ApiEditModell(JuiceVM modell, bool isEditing)
        {
            if (modell == null) return false;
            string myUrl = isEditing ? url + "mod" : url + "add";

            Dictionary<string, string> postData = new Dictionary<string, string>();
            if (isEditing) postData.Add(nameof(JuiceVM.ID), modell.ID.ToString());
            postData.Add(nameof(JuiceVM.BRAND), modell.BRAND);
            postData.Add(nameof(JuiceVM.MADEINCOUNTRY), modell.MADEINCOUNTRY);
            postData.Add(nameof(JuiceVM.FRUITS), modell.FRUITS);
            postData.Add(nameof(JuiceVM.PRICE), modell.PRICE.ToString());
            postData.Add(nameof(JuiceVM.PRODUCTION), modell.PRODUCTION.ToString());
            postData.Add(nameof(JuiceVM.PROFIT), modell.PROFIT.ToString());

            string json = client.PostAsync(myUrl, new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
            JObject obj = JObject.Parse(json);
            return (bool)obj["OperationResult"];
        }
        public void ApiEditModell(JuiceVM modell, Func<JuiceVM, bool> editor)
        {
            JuiceVM clone = new JuiceVM();
            if (modell != null) clone.CopyFrom(modell);
            bool? success = editor?.Invoke(clone);
            if (success == true)
            {
                if (modell != null) success = ApiEditModell(clone, true);
                else success = ApiEditModell(clone, false);
            }
            SendMessage(success == true);
        }
    }
}

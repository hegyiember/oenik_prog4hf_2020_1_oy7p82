﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace JuiceShop.Wpf
{
    class MainVM : ViewModelBase
    {
        private MainLogic logic;
        private JuiceVM selectedJuice;
        public JuiceVM SelectedJuice { get { return selectedJuice; } set { Set(ref selectedJuice, value); } }
        private ObservableCollection<JuiceVM> juiceVMs;
        public ObservableCollection<JuiceVM> JuiceVMs { get { return juiceVMs; } set { Set(ref juiceVMs, value); } }
        public ICommand AddCmd { get; private set; }
        public ICommand DelCmd { get; private set; }
        public ICommand ModCmd { get; private set; }
        public ICommand LoadCmd { get; private set; }

        public Func<JuiceVM, bool> EditorFunc { get; set; }

        public MainVM()
        {
            this.logic = new MainLogic();

            DelCmd = new RelayCommand(() => logic.ApiDelModell(SelectedJuice));
            AddCmd = new RelayCommand(() => logic.ApiEditModell(null, EditorFunc));
            ModCmd = new RelayCommand(() => logic.ApiEditModell(SelectedJuice, EditorFunc));
            LoadCmd = new RelayCommand(() => JuiceVMs = new ObservableCollection<JuiceVM>(logic.ApiGetModells()));


        }
    }
}

﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JuiceShop.Wpf
{
    class JuiceVM : ObservableObject
    {
        private int id;
        public int ID { get { return id; } set { Set(ref id, value); } }
        private string brand;
        public string BRAND { get { return brand; } set { Set(ref brand, value); } }
        private string madeincountry;
        public string MADEINCOUNTRY { get { return madeincountry; } set { Set(ref madeincountry, value); } }
        private string fruits;
        public string FRUITS { get { return fruits; } set { Set(ref fruits, value); } }
        private int price;
        public int PRICE { get { return price; } set { Set(ref price, value); } }
        private int production;
        public int PRODUCTION { get { return production; } set { Set(ref production, value); } }
        private int profit;
        public int PROFIT { get { return profit; } set { Set(ref profit, value); } }
        

        public void CopyFrom(JuiceVM be)
        {
            if (be == null) return;
            this.ID = be.ID;
            this.BRAND = be.BRAND;
            this.MADEINCOUNTRY = be.MADEINCOUNTRY;
            this.FRUITS = be.FRUITS;
            this.PRICE = be.PRICE;
            this.PRODUCTION = be.PRODUCTION;
            this.PROFIT = be.PROFIT;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace JuiceShop.Web.Models
{
    public class Juice
    {
        [Display(Name = "Juice ID")]
        [Required]
        public int ID { get; set; }
        [Display(Name = "Juice BRAND")]
        [Required]
        [StringLength(100, MinimumLength = 5)]
        public string BRAND { get; set; }
        [Display(Name = "Juice MADEINCOUNTRY")]
        [Required]
        [StringLength(100, MinimumLength = 5)]
        public string MADEINCOUNTRY { get; set; }
        [Display(Name = "Juice FRUITS")]
        [Required]
        [StringLength(100, MinimumLength = 5)]
        public string FRUITS { get; set; }
        [Display(Name = "Juice PRODUCTION")]
        [Required]
        public int PRODUCTION { get; set; }
        [Display(Name = "Juice PRICE")]
        [Required]
        public int PRICE { get; set; }
        [Display(Name = "Juice PROFIT")]
        [Required]
        public int PROFIT { get; set; }
    }
}
﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JuiceShop.Web.Models
{
    public class MapperFactory
    {
        public static IMapper CreateMapper()
        {
            var mapper = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<JuiceShop.Data.JUICE, JuiceShop.Web.Models.Juice>().
                    ForMember(ind => ind.ID, cel => cel.MapFrom(src => src.ID)).
                    ForMember(ind => ind.BRAND, cel => cel.MapFrom(src => src.BRAND)).
                    ForMember(ind => ind.MADEINCOUNTRY, cel => cel.MapFrom(src => src.MADEINCOUNTRY)).
                    ForMember(ind => ind.FRUITS, cel => cel.MapFrom(src => src.FRUITS)).
                    ForMember(ind => ind.PRICE, cel => cel.MapFrom(src => src.PRICE)).
                    ForMember(ind => ind.PRODUCTION, cel => cel.MapFrom(src => src.PRODUCTION)).
                    ForMember(ind => ind.PROFIT, cel => cel.MapFrom(src => src.PROFIT));
            });
            return mapper.CreateMapper();
        }
    }
}
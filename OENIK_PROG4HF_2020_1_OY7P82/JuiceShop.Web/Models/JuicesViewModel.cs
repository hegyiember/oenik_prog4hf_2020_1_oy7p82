﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JuiceShop.Web.Models
{
    public class JuicesViewModel
    {
        public Juice EditedJuice { get; set; }
        public List<Juice> AllJuices { get; set; }
    }
}
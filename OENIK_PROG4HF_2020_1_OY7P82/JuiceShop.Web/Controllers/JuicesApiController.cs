﻿using AutoMapper;
using JuiceShop.Data;
using JuiceShop.Logic;
using JuiceShop.Repository;
using JuiceShop.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace JuiceShop.Web.Controllers
{
    public class JuicesApiController : ApiController
    {
        public class ApiResult
        {
            public bool OperationResult { get; set; }
        }
        IJuiceLogic logic;
        IMapper mapper;
        public JuicesApiController()
        {
            DBEntities ctx = new DBEntities();
            JuiceRepository repository = new JuiceRepository(ctx);
            logic = new JuiceLogic(repository);
            mapper = MapperFactory.CreateMapper();
        }
        [ActionName("all")]
        [HttpGet]
        // GET api/JuicesApi/all
        public IEnumerable<Models.Juice> GetAll()
        {
            var juices = logic.GetAllJuices();
            return mapper.Map<IList<Data.JUICE>, List<Models.Juice>>(juices);
        }
        // GET api/JuicesApi/del/5
        [ActionName("del")]
        [HttpGet]
        public ApiResult DelOneJuice(int id)
        {
            return new ApiResult() { OperationResult = logic.RemoveJuice(id) };
        }
        // POST api/JuicesApi/add + juice
        [HttpPost]
        [ActionName("add")]
        public ApiResult AddOneCar(Juice juice)
        {
            logic.AddJuice(juice.BRAND, juice.MADEINCOUNTRY, juice.FRUITS, juice.PRODUCTION, juice.PRICE, juice.PROFIT);
            return new ApiResult() { OperationResult = true };
        }
        // POST api/JuicesApi/mod  + juice
        [HttpPost]
        [ActionName("mod")]
        public ApiResult ModOneCar(Juice juice)
        {
            return new ApiResult()
            {
                OperationResult = logic.ChangeJuice(juice.ID, juice.BRAND, juice.MADEINCOUNTRY, juice.FRUITS, juice.PRODUCTION, juice.PRICE, juice.PROFIT)
            };
        }
    }
}

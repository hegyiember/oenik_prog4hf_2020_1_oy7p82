﻿using AutoMapper;
using JuiceShop.Data;
using JuiceShop.Logic;
using JuiceShop.Repository;
using JuiceShop.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JuiceShop.Web.Controllers
{
    public class JuicesController : Controller
    {
        IJuiceLogic logic;
        IMapper mapper;
        JuicesViewModel model;
        public JuicesController()
        {
            DBEntities ctx = new DBEntities();
            JuiceRepository repository = new JuiceRepository(ctx);
            logic = new JuiceLogic(repository);
            mapper = MapperFactory.CreateMapper();
            model = new JuicesViewModel();
            model.EditedJuice = new Juice();
            model.AllJuices = mapper.Map<IList<Data.JUICE>, List<Models.Juice>>(logic.GetAllJuices());
        }

        private Juice GetJuice(int d)
        {
            var kek = logic.GetOneJuice(d);
            return mapper.Map<Data.JUICE, Models.Juice>(kek);
        }
        // GET: Juices
        public ActionResult Index()
        {
            ViewData["editAction"] = "AddNew";
            return View("JuicesIndex", model);
        }
        // GET: Juices/Details/5
        public ActionResult Details(int id)
        {
            return View("JuicesDetails", GetJuice(id));
        }
        public ActionResult Remove(int id)
        {
            TempData["editResult"] = "Delete FAIL";
            if (logic.RemoveJuice(id)) TempData["editResult"] = "Delete OK";
            return RedirectToAction("Index");
        }
        public ActionResult Edit(int id)
        {
            ViewData["editAction"] = "Edit";
            model.EditedJuice = this.GetJuice(id);
            return View("JuicesIndex", model);
        }
        [HttpPost]
        public ActionResult Edit(Juice juice, string editAction)
        {
            if (ModelState.IsValid && juice != null)
            {
                TempData["editResult"] = "Edit OK";
                if (editAction == "AddNew")
                {
                    logic.AddJuice(juice.BRAND, juice.MADEINCOUNTRY, juice.FRUITS, juice.PRODUCTION, juice.PRICE, juice.PROFIT);
                }
                else
                {
                    bool succss = logic.ChangeJuice(juice.ID, juice.BRAND, juice.MADEINCOUNTRY, juice.FRUITS, juice.PRODUCTION, juice.PRICE, juice.PROFIT);
                    if (!succss) TempData["editResult"] = "Edit FAIL";
                }
                return RedirectToAction(nameof(Index));
            }
            else
            {
                ViewData["editAction"] = "Edit";
                model.EditedJuice = juice;
                return View("JuicesIndex", model);
            }
        }

    }
}

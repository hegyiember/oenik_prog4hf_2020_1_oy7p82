﻿using JuiceShop.Data;
using JuiceShop.Logic;
using JuiceShop.Repository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JuiceShop.Program
{
    class Program
    {
        static void Main(string[] args)
        {
            DBEntities dbContext = new DBEntities();
            JuiceRepository repository = new JuiceRepository(dbContext);
            JuiceLogic juiceLogic = new JuiceLogic(repository);
            Console.WriteLine("Test Started, press enter");
            Console.ReadLine();

            Console.WriteLine("Basic database");
            OutProd(juiceLogic);
            Console.ReadLine();

            Console.WriteLine("ADD");
            juiceLogic.AddJuice("TEST1", "RUS", "pineapple", 100, 2, 20);
            OutProd(juiceLogic);
            Console.ReadLine();

            Console.WriteLine("MOD");
            juiceLogic.ChangeJuice(5, "TESTMOD", "GER", "kangororr", 100, 2, 20);
            OutProd(juiceLogic);
            Console.ReadLine();

            Console.WriteLine("DEL");
            juiceLogic.RemoveJuice(5);
            OutProd(juiceLogic);
            Console.ReadLine();
        }

        private static void OutProd(JuiceLogic juiceLogic)
        {
            var temp = juiceLogic.GetAllJuices();
            foreach (var x in temp)
            {
                Console.WriteLine(x.ID + "\t" + x.BRAND + "\t" + x.MADEINCOUNTRY + "\t" + x.FRUITS + "\t" + x.PRICE + "\t" + x.PRODUCTION + "\t" + x.PROFIT);
            }
        }
    }
}

﻿using JuiceShop.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JuiceShop.Repository
{
    public class JuiceRepository : IJuiceRepository
    {
        protected DBEntities ctx;
        public JuiceRepository(DBEntities ctx)
        {
            this.ctx = ctx;
        }
        public void ChangePrice(int id, int newprice)
        {
            var jiuce = GetOne(id);
            jiuce.PRICE = newprice;
            ctx.SaveChanges();
        }
        public bool ChangeJuice(int id, string brand, string newmadeincountry, string newfruits, int newproduction, int newprice, int newprofit)
        {
            JUICE entity = GetOne(id);
            if (entity == null) return false;
            entity.BRAND = brand;
            entity.MADEINCOUNTRY = newmadeincountry;
            entity.FRUITS = newfruits;
            entity.PRODUCTION = newproduction;
            entity.PRICE = newprice;
            entity.PROFIT = newprofit;
            ctx.SaveChanges();
            return true;
        }

        public IList<JUICE> GetAll()
        {
            return ctx.Set<JUICE>().ToList();
        }

        public JUICE GetOne(int id)
        {
            return GetAll().SingleOrDefault(x => x.ID == id);
        }

        public void Insert(JUICE entity)
        {
            ctx.Set<JUICE>().Add(entity);
            ctx.SaveChanges();
        }

        public bool Remove(int id)
        {
            JUICE entity = GetOne(id);
            if (entity == null) return false;
            ctx.Set<JUICE>().Remove(entity);
            ctx.SaveChanges();
            return true;
        }
    }
}

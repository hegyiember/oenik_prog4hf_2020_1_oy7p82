﻿using JuiceShop.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JuiceShop.Repository
{
    public interface IJuiceRepository
    {
        JUICE GetOne(int id);
        IList<JUICE> GetAll();

        bool Remove(int id);
        void Insert(JUICE entity);
        bool ChangeJuice(int id, string brand, string newmadeincountry, string newfruits, int newproduction, int newprice, int newprofit);
    }
}

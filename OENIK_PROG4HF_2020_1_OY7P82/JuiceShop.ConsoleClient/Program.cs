﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace JuiceShop.ConsoleClient
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("WAITING...");
            Console.ReadLine();

            string url = "http://localhost:65454/api/JuicesApi/";

            using (HttpClient client = new HttpClient())
            {
                string json = client.GetStringAsync(url + "all").Result;
                var list = JsonConvert.DeserializeObject<List<Juice>>(json);
                foreach (var x in list) Console.WriteLine(x.ID + "\t" + x.BRAND + "\t" + x.MADEINCOUNTRY + "\t" + x.FRUITS + "\t" + x.PRICE + "\t" + x.PRODUCTION + "\t" + x.PROFIT);
                Console.ReadLine();

                Dictionary<string, string> postData;
                string response;

                postData = new Dictionary<string, string>();
                postData.Add(nameof(Juice.BRAND), "ConsoleApiTester");
                postData.Add(nameof(Juice.MADEINCOUNTRY), "Turulföld");
                postData.Add(nameof(Juice.FRUITS), "Igazmagyar Gyöngyalma");
                postData.Add(nameof(Juice.PRICE), "10");
                postData.Add(nameof(Juice.PROFIT), "99");
                postData.Add(nameof(Juice.PRODUCTION), "50");
                response = client.PostAsync(url + "add", new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("ADD: " + response);
                Console.WriteLine("ADD: " + json);
                Console.ReadLine();

                int modellId = JsonConvert.DeserializeObject<List<Juice>>(json).Last().ID;
                postData = new Dictionary<string, string>();
                postData.Add(nameof(Juice.ID), modellId.ToString());
                postData.Add(nameof(Juice.BRAND), "Edittester");
                postData.Add(nameof(Juice.MADEINCOUNTRY), "Turulföld");
                postData.Add(nameof(Juice.FRUITS), "Igazmagyar Gyöngyalma");
                postData.Add(nameof(Juice.PRICE), "10");
                postData.Add(nameof(Juice.PROFIT), "99");
                postData.Add(nameof(Juice.PRODUCTION), "50");
                response = client.PostAsync(url + "mod", new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("MOD: " + response);
                Console.WriteLine("ALL: " + json);
                Console.ReadLine();

                response = client.GetStringAsync(url + "del/" + modellId).Result;
                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("DEL: " + response);
                Console.WriteLine("ALL: " + json);
                Console.ReadLine();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JuiceShop.ConsoleClient
{
    public class Juice
    {
        public int ID { get; set; }
        public string BRAND { get; set; }
        public string MADEINCOUNTRY { get; set; }
        public string FRUITS { get; set; }
        public int PRICE { get; set; }
        public int PRODUCTION { get; set; }
        public int PROFIT { get; set; }
    }
}

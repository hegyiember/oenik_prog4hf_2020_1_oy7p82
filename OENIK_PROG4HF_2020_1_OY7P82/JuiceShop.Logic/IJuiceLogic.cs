﻿using JuiceShop.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JuiceShop.Logic
{
    public interface IJuiceLogic
    {
        void AddJuice(string brand, string newmadeincountry, string newfruits, int newproduction, int newprice, int newprofit);
        JUICE GetOneJuice(int id);
        IList<JUICE> GetAllJuices();
        bool ChangeJuice(int id, string brand, string madeincountry, string fruits, int production, int price, int profit);
        bool RemoveJuice(int id);
    }
}

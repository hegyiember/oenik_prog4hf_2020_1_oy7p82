﻿using JuiceShop.Data;
using JuiceShop.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JuiceShop.Logic
{
    public class JuiceLogic : IJuiceLogic
    {
        IJuiceRepository repository;
        public JuiceLogic(IJuiceRepository IRepository)
        {
            this.repository = IRepository;
        }

        public void AddJuice(string brand, string newmadeincountry, string newfruits, int newproduction, int newprice, int newprofit)
        {
            JUICE newjuice = new JUICE();
            newjuice.BRAND = brand;
            newjuice.MADEINCOUNTRY = newmadeincountry;
            newjuice.FRUITS = newfruits;
            newjuice.PRODUCTION = newproduction;
            newjuice.PRICE = newprice;
            newjuice.PROFIT = newprofit;
            repository.Insert(newjuice);
        }

        public bool ChangeJuice(int id, string brand, string madeincountry, string fruits, int production, int price, int profit)
        {
            return this.repository.ChangeJuice(id, brand, madeincountry, fruits, production, price, profit);
        }

        public IList<JUICE> GetAllJuices()
        {
            return this.repository.GetAll();
        }

        public JUICE GetOneJuice(int id)
        {
            return repository.GetOne(id);
        }

        public bool RemoveJuice(int id)
        {
            return this.repository.Remove(id);
        }
    }
}
